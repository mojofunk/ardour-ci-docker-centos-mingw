#! /bin/bash

. env.sh

sudo docker run -i -t --rm -v $HOST_DEVEL_DIR:$CONTAINER_DEVEL_DIR:Z \
-v $HOST_SHARE_DIR:$CONTAINER_SHARE_DIR:Z $NAME '/bin/bash'
