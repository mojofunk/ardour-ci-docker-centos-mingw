A Centos 7 based docker image for reproducible cross compiled
Windows/mingw builds of Ardour. 

Start the docker deamon

$ sudo systemctl start docker

Make the Docker container

$ ./make-docker-image.sh

Run bash in Docker image

$ ./run-bash.sh

Navigate to /var/devel/abuild, which requires setting up a devel
directory on the host, cloning abuild in it and then setting
HOST_DEVEL_DIR appropriately in the environment.

$ ./abuild.sh build ardour
