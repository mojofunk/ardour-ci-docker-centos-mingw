#!/bin/bash

NAME=ardour-ci-docker-centos7-mingw

HOST_DEVEL_DIR=~/docker-share/devel
CONTAINER_DEVEL_DIR=/var/devel

HOST_SHARE_DIR=~/docker-share/$NAME
CONTAINER_SHARE_DIR=/var/tmp
