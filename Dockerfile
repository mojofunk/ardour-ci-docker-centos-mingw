FROM centos:7

#ADD ardour-build-tools/win/x-mingw.sh /

RUN yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm

RUN yum group install -y 'Development Tools'

RUN yum install -y \
vim \
git \
mingw*gcc \
mingw*gcc-c++ \
mingw*pkg-config \
glib2-devel \
ed \
cmake \
python34 \
&& yum clean all

#CMD [ "bin/bash", "x-mingw.sh" ]
